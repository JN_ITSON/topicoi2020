const express = require('express');
const router = express.Router();

const userController = require('../controller/userController');


router.get('/', userController.list);
router.post('/add', userController.add);



module.exports = router;