const express = require('express');
const myConn = require('express-myconnection');
const mysql = require('mysql');
const path = require('path');

const app = express();

//importar rutas
const userRoutes = require('./routes/user');
const { urlencoded } = require('express');

//Config Express
app.set('port', process.env.PORT || 3000);
app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'));

//Middleware
const conn = {
    host:'localhost',
    user:'root',
    password: '',
    database: 'expresscrud',
    port: 3306
};

app.use(myConn(mysql,conn,'single'));
app.use(express.urlencoded({extended: false})); //Extends false para formularios sin imagenes o archivos

//Rutas
app.get('/',userRoutes);

//Archivos Estaticos (CSS, JS, ETC... )
app.use(express.static(path.join(__dirname,'public')));


app.listen(app.get('port'), ()=>{
    console.log(`Servidor iniciado en el puerto ${app.get('port')}`);
});