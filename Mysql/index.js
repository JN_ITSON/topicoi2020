const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');

const app = express();
const PORT = process.env.PORT || 3000;

//conexion a mysql
let bd = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'api'
});



app.set('port',PORT);
//Le permitimos a app utilizar formatos JSON para enviar o recibir datos
app.use(bodyParser.json());


app.listen(app.get('port'), ()=>{
    console.log(`El servidor está corriendo en el puerto ${PORT}`);
});

app.get('/',(req, res)=>{
    res.send('Bienvenido, gracias por utilizar mi API');
});

//API CRUD
app.get('/users',(req, res)=>{

    const query = 'SELECT * FROM user';
    if(bd){
        bd.query(query,(error, data)=>{
            if(error){
                throw error;
            }else{
                res.status(200).json(data);
            }
        });
    }
    //res.send('Endpoint obtener usuarios');
});

app.get('/users/:id',(req, res)=>{
    const { id } = req.params;
    const query = `SELECT * FROM user WHERE id = ${id}`;
    if(bd){
        bd.query(query,(error, data)=>{
            if(error){
                throw error;
            }else{
                res.status(200).json(data);
            }
        });
    }
    //res.send('Endpoint consultar un usuario '+id);
});

app.post('/users',(req, res)=>{ //post = agregar
    //const nombre = req.body.nombre;
    const { nombre, email, carrera, semestre, edad , contrasena } = req.body;
    const user = {
        nombre: nombre,
        email: email,
        carrera: carrera,
        semestre: semestre,
        edad: edad,
        contrasena: contrasena
    };

   // console.log(user);

    const query = `INSERT INTO user SET ? `;
    if(bd){
        bd.query(query,user, (error, data, fields)=>{
            if(error){
                throw error;
            }else{
                console.log(data.insertId);
                const respuesta = {
                    respuesta : 'OK',
                    id: data.insertId
                }
                res.status(200).json(respuesta);
            }
        });
    }
    
   // res.send('Endpoint agregar un usuario');
});

app.put('/users/:id',(req, res)=>{
    const id = req.params.id;
    const { nombre, email, carrera, semestre, edad , contrasena } = req.body;
    const user = {
        nombre: nombre,
        email: email,
        carrera: carrera,
        semestre: semestre,
        edad: edad,
        contrasena: contrasena
    };

    const query = `UPDATE user SET ? WHERE id = ${id}` ;
    if(bd){
        bd.query(query,user, (error, data)=>{
            if(error) throw error
            else{
                const respuesta = {
                    respuesta : 'El usuario se actualizo correctamente'
                };

                res.status(200).json(respuesta);
            }
        });
    }

    // const { id } = req.params;
   // res.send('Endpoint editar un usuario '+id);
});

app.delete('/users/:id',(req,res)=>{
    // const id = req.params.id;
    const { id } = req.params;
    const query = `DELETE FROM user WHERE id = ${id}`;
    
    if(bd){
        bd.query(query, (error, data)=>{
            if(error) throw error;
            else{
                const respuesta = {
                    respuesta: 'El usuario se elimino correctamente'
                };

                res.status(200).json(respuesta);

            }
        })
    }
    
    //res.send('Endpoint editar un usuario '+id);
});