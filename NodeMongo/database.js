const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const URI_DB = 'mongodb://localhost:27017/user';
mongoose.connect(URI_DB,{
    useNewUrlParser : true,
    useUnifiedTopology : true,
    useCreateIndex: true
}, (err)=>{
    if(!err)
        console.log('MongoDB se ha conectado correctamentes')
    else
        console.log(`Error MongoDB: ${err}`)
})