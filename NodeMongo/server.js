const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 3000;

const db = require('./database');
var User = require('./user.model');

app.use(express.urlencoded({extended: false}));
app.use(express.json());

//GETs 
app.get('/users',(req,res)=>{
    User.find((err, users)=>{
        if(err) return res.status(500).send(err.message);
        res.status(200).jsonp(users);
    });
});

app.get('/users/:id',(req, res)=>{
    User.findById({_id: req.params.id},(err, user)=>{
        if(err) return res.status(500).send(err.message);
        res.status(200).jsonp(user);
    })
})

//ADD
app.post('/users', async (req, res)=>{
    var user = new User({
        nombre: req.body.nombre,
        edad: req.body.edad,
        email: req.body.email,
        contrasena: req.body.contrasena,
        semestre: req.body.semestre
    });

    user.save((err,data)=>{
        if(err) return res.status(500).send(err.message);
        res.status(200).jsonp(data);
    });

});

//Update
app.put('/users/:id',(req,res)=>{
    User.findById({_id: req.params.id},(err, user)=>{
        user.nombre = req.body.nombre;
        user.edad = req.body.edad;
        user.semestre = req.body.semestre;
        user.email = req.body.email;
        user.contrasena = req.body.contrasena;

        user.save((err)=>{
            if(err) return res.status(500).send(err.message);
            res.status(200).jsonp(user);
        })

    })
})


//Delete
app.delete('/users/:id',(req,res)=>{
    User.findById({_id: req.params.id},(err, user)=>{
        
        user.remove((err)=>{
            if(err) return res.status(500).send(err.message);
            res.status(200).jsonp({mensaje:"Se ha eliminado correctamente."});
        })

    })
})


app.listen(port, ()=>{
    console.log(`Servidor iniciado en el puerto ${port}`);
});