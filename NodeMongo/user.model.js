const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    nombre : {type: String, required: true},
    edad : {type: Number},
    email :  {type: String, required: true},
    contrasena :  {type: String, required: true},
    semestre : {type: Number}
});

module.exports = mongoose.model('User',UserSchema);