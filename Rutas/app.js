const express = require('express')
const app = express()
const port = 3000

//declaración de variables
const homeRoutes = require('./routes/home');
const userRoutes = require('./routes/user')
const productRoutes = require('./routes/producto')



//Rutas 
app.use('/',homeRoutes);
app.use('/user',userRoutes);
app.use('/product',productRoutes);


/*
/ - home

/ingresos/ - add , edit , eliminar 
/egresos/  - add , edit , eliminar

*/

app.listen(port, () => console.log(`Example app listening on port port!`))