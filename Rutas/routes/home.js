const express = require('express')

const router = express.Router();

router.get('/',(req,res)=>{
    res.send("Pantalla principal de home");
})

router.get('/add',(req,res)=>{
    res.send('Se ha agregado a Home correctamente.')
})

module.exports = router;