const express = require('express')
const router = express.Router();

router.get('/', (req,res)=>{
    res.send('Bienvenido a la lista de usuarios')
})

router.get('/add',(req,res)=>{
    res.send('Se ha agregado el usuario correctamente.')
})

module.exports = router;

/*

/

user/add
user/edit
user/delete
user/:id
user/

producto/add
producto/edit
producto/delete
producto/:id
producto/


*/