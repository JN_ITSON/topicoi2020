// const http = require('http')
const express = require('express')
const app = express();
const port = 3000;

const ws = require('./ws'); //ws web socket 


app.listen(port, function() { // this
    ws(this); //this dentro de está función se refiere al servidor
    console.log(`El servidor se ha iniciado en el puerto ${port}`);
})