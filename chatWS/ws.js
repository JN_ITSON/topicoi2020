const io = require('socket.io');

const chat = (servidor) =>{

    /* 
            ws on -> Está esperando o escuchando por una instrucción
            socket emit-> Para enviar un mensaje al socket
            socket broadcast emit -> Envia mensaje a todos los sockets excluyendo al socket sel.
            ws broadcast emit -> Envia mensaje a todos los sockets

    */
    const ws = io(servidor,{
        cors: {
            origin: "http://localhost:2000",
            methods: ["GET","POST"]
        }
    });

    var users  = []; //almacenamos toda la lista de usuarios conectados

    //Metodo para escuchar nuevas conexiones
    ws.on('connection', (socket)=>{
        let user = {
            "socketID": socket.id,
            "nombre": ""
        };


        socket.on('login',(nombre)=>{
            user.nombre = nombre;
            // console.log(user);

            var pos = users.indexOf(user);

            if(pos === -1){
                users.push(user);
                //Pasar la lista de usuarios al que se conecta 
                socket.emit('logged',user.socketID);
                socket.emit('userLst',users); //envia mensaje al usuario especifico

                //Pasar el nuevo usuario al que ya está logeado
                socket.broadcast.emit('newUser',user);

                console.log(users);
            }

        });

        socket.on('enviarMSG',(nombre,msg)=>{
            console.log(nombre,msg);
            socket.broadcast.emit('recibirMSG',nombre,msg);
        });

        socket.on('disconnect',()=>{

            const pos = users.indexOf(user);

            if(pos !== -1){
                users.splice(pos,1);//removiendo usuario de la lista
                socket.broadcast.emit('userDisconnected',user);
            }

        });

    });

};

module.exports = chat;