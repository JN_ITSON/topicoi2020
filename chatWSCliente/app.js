const express = require('express')
const app = express()
const port = 2000
const path = require('path')

app.set('view engine','ejs')


//archivos estaticos
// app.use(express.static(path.join(__dirname,"public")));

app.get('/', (req, res) => res.render('chat'))
app.listen(port, () => console.log(`Example app listening on port ${port}! `))