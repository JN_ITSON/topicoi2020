const mongoose = require('mongoose');
const db = require('../db');

var User = require('../models/user.model');

const controller = {};

controller.list = (req,res)=>{

    User.find((err,usuarios)=>{
        if(err) console.log('Error: '+err);

        res.render('home',{
            users: usuarios
        });

    })

}

controller.save = (req, res)=>{
    var newUser = User({
        nombre: req.body.nombre,
        confirmado: 0
    });


    newUser.save((err,usersaved)=>{
        if(err) console.log(err);
        console.log('Guardado con exito!.. ',usersaved);
        res.redirect('/');
    })
}

controller.accept = (req, res)=>{
    User.findById({_id: req.params.id }, (err, user)=>{
        if(err) console.log(err);
        //Modificar el usuario

        user.confirmado = 1;

        user.save((err,usersaved)=>{
            if(err) console.log(err);
            res.redirect('/');
        })
    });
}

controller.reject = (req, res)=>{
    User.findById({_id: req.params.id }, (err, user)=>{
        if(err) console.log(err);
        //Modificar el usuario

        user.confirmado = 2;

        user.save((err,usersaved)=>{
            if(err) console.log(err);
            res.redirect('/');
        })
    });
}



module.exports = controller;