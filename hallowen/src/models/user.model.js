const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    nombre: {type: String, required:true},
    confirmado: {type: Number, required: false}
});

module.exports = mongoose.model('User',userSchema);