const express = require('express')
const router = express.Router();

const homeController = require('../controllers/homeController');


router.get('/',homeController.list);
router.post('/add',homeController.save);
router.get('/accept/:id',homeController.accept);
router.get('/reject/:id', homeController.reject);

module.exports = router;