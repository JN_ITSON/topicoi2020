const express = require('express')
const app = express()
const port = 3000
const path = require('path')

//conf engine
app.use(express.urlencoded({extended: false}))
app.use(express.json())

app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'))


//declaracion de variables rutas
const homeRoutes = require('./routes/home');

//rutas
app.use('/',homeRoutes);


//Archivos Estaticos
app.use(express.static(path.join(__dirname,'public')));

app.listen(port, () => console.log(`Example app listening on port port!`))