const express = require('express')
const passport = require('passport')
const cookieParser = require('cookie-parser')
const session = require('express-session');
const { use } = require('passport');
const passportLocal = require('passport-local').Strategy;

//Config
const app = express()
const port = 3000
app.set('view engine','ejs'); // views

//Middlewares
app.use(express.urlencoded({extended:false}));
app.use(cookieParser('mi secreto'))
app.use(session({
    secret: 'mi secreto',
    resave: true,
    saveUninitialized: true
}))

app.use(passport.initialize())
app.use(passport.session())

/*

    passport.use(new passportLocal({
        usernameField: 'email',
        passwordField: 'contrasena'
    },(username,password,done)=>{
        ....
    }))


*/

passport.use(new passportLocal( (username,password, done)=>{

    //logica de login (Se conecta a su base de datos)
    if(username == 'jorge' && password == '1234'){
        return done(null,{id:1, name:'Jorge Norzagaray'})
    }
    done(null,false)
} ));


//serializamos los objetos de passport
// {id:1, name:'Jorge Norzagaray'}  Deserializado
// 1   Serializado
passport.serializeUser((user,done)=>{
    done(null, user.id)
})


//Deserializar
passport.deserializeUser((id,done)=>{
    //Consulta a la base de datos el usuario por medio del id
    done(null,{id:1, name:'Jorge Norzagaray'})
})

//Rutas
app.get('/',
     (req,res,next)=>{ //Middleware
        console.log(req.user); 
        //Cuando no esté logueado /login
        if(req.isAuthenticated()) return next();
                
         res.redirect('/login');
     } ,
     
     (req, res) => {
    

    //Logeado
    res.send('hola '+req.user.name);

})

app.get('/login',(req,res)=>{
    res.render('login')
})

app.get('/logout',(req,res)=>{
    req.logout();
    res.redirect('/login');
})

app.post('/login', passport.authenticate('local',{
    successRedirect: '/',
    failureRedirect: '/login'
}) )


//Servidor
app.listen(port, () => console.log(`Example app listening on port port!`))